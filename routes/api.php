<?php

use App\Http\Controllers\Api\Management\RepoContribution as ManagementRepoContribution;
use App\Http\Controllers\RepoContributions;
use App\Http\Controllers\Api\Management\Repo;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('rest')->middleware(['auth:api'])->group(function () {
    Route::apiResource('repo', Repo::class);
    Route::apiResource('repo-contribution', ManagementRepoContribution::class);

    #Route::post('repo-contribution/auto-resolve', [RepoContributions::class, 'storeWithResolver']);
});

Route::prefix('🌐')->group(function () {
    Route::get('repo-contribution', [RepoContributions::class, 'serve']);
});
