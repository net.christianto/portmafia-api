<?php

use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::inertia('/', 'home/index');

Route::prefix('auth')->group(function () {
    Route::get('login', [Auth::class, 'login'])->name('auth:login');
    Route::post('login', [Auth::class, 'loginDo']);
    Route::get('callback', [Auth::class, 'callback'])->name('auth:callback');
    Route::any('logout', [Auth::class, 'logout'])->name('auth:logout');
});

Route::middleware('auth')->prefix('admin')->group(function () {
    Route::inertia('/', 'admin/home/index')
        ->name('admin:home');
});
