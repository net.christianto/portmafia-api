import React, { HTMLAttributes } from 'react'
import styles from "./auth.module.scss";
import { Col, Container, Row } from 'react-bootstrap';
import classNames from 'classnames';
import { When } from 'react-if';
import BaseLayout from '../base/base-layout';

export interface AuthLayoutProps {
    title: string,
    subtitle?: string,
    children: HTMLAttributes<HTMLElement>['children'],

    leftClassName?: string,
    rightClassName?: string,
}

function AuthLayout({ title, subtitle, children, leftClassName, rightClassName }: AuthLayoutProps) {
    return (
        <BaseLayout>
            <Container fluid className={styles.container}>
                <Row>
                    <Col md={8} className={classNames(leftClassName, styles.container__left)}>
                        <div className={styles.titler}>
                            <div>
                                <p className={styles.titler__branding}>Portmafia</p>
                                <h1>{title}</h1>
                                <When condition={subtitle}>
                                    <p className='lead'>{subtitle}</p>
                                </When>
                            </div>
                        </div>
                    </Col>
                    <Col className={classNames(rightClassName, styles.container__right)}>
                        {children}
                    </Col>
                </Row>
            </Container>
        </BaseLayout>
    )
}

export default AuthLayout
