
import React, { HTMLAttributes } from 'react'
import "@/styles/app.scss"

function BaseLayout({ children, ...props }: HTMLAttributes<HTMLDivElement>) {
    return (<>
        <div {...props}>
            {children}
        </div>
    </>
    )
}

export default BaseLayout
