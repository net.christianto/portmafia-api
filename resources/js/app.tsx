import { createInertiaApp } from "@inertiajs/react";
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import React from "react";
import { createRoot } from "react-dom/client";

createInertiaApp({
    title(title) {
        if (title) {
            return `${title} | Portmafia | Christianto`
        }
        return 'Portmafia | Christianto';
    },
    resolve: name => resolvePageComponent(`./pages/${name}.tsx`, import.meta.glob('./pages/**/*.tsx')),
    setup({ el, App, props }) {
        createRoot(el).render(<React.StrictMode><App {...props} /></React.StrictMode>)
    },
})
