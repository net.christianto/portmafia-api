export interface GenericApiResponse<T = any> {
    data: T;
    status: boolean;
    message: string | null;
}

export interface PaginationLinks {
    links: {
        first: string | null;
        last: string | null;
        prev: string | null;
        next: string | null;
    };
}

export interface CursoredResponse extends PaginationLinks {
    meta: {
        path: string;
        per_page: number;
        next_cursor: string | null;
        prev_cursor: string | null;
    };
}

export interface PaginatedResponse extends PaginationLinks {
    meta: {
        current_page: number;
        from: number;
        last_page: number;
        links: Array<{ url: string | null; label: string; active: boolean }>;
        path: string;
        per_page: number;
        to: number;
        total: number;
    };
}

export type CursoredApiResponse<T = any> = GenericApiResponse<Array<T>> & CursoredResponse;

export type PaginatedApiResponse<T = any> = GenericApiResponse<Array<T>> & PaginatedResponse;

export type GenericPaginatedResponse<T = any> = CursoredApiResponse<T> | PaginatedApiResponse<T>;
