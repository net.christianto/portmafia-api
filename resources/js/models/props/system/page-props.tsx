import { User } from "@/models/user";
import { PageProps as InertiaPageProps } from "@inertiajs/inertia";

export interface PageProps extends InertiaPageProps {
    user: User
    errors?: { [key: string]: string[] }
    baseUrl: string
}
