import { Visibility, VisibilityOff } from '@mui/icons-material'
import React from 'react'
import { BulkDeleteButton, BulkExportButton, BulkUpdateButton } from 'react-admin'

function HideUnhideButtons() {
    return (
        <>
            <BulkUpdateButton mutationMode='pessimistic' label="Show" data={{ is_hidden: false }} icon={<Visibility />} />
            <BulkUpdateButton mutationMode='pessimistic' label="Hide" data={{ is_hidden: true }} icon={<VisibilityOff />} />
            <BulkDeleteButton mutationMode='pessimistic' />
            <BulkExportButton />
        </>
    )
}

export default HideUnhideButtons
