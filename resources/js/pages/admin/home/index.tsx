import React from 'react'
import { Admin, Resource } from 'react-admin'
import RepoContributionList from './resources/repo-contributions/list'
import RepoList from './resources/repo/list'
import { QueryClient } from 'react-query';
import RepoContributionsEdit from './resources/repo-contributions/edit'
import RepoEdit from './resources/repo/edit'
import RepoCreate from './resources/repo/create'
import RepoContributionsCreate from './resources/repo-contributions/create'
import { useAuthProvider } from '@/util/admin/auth-provider'
import { createDataProvider } from '@/util/laravel-data-provider';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 60 * 1e3,
            retry: 3,
            refetchOnWindowFocus: true,
        },
    },
});
const dataProvider = createDataProvider();

function AdminIndex() {
    const authProvider = useAuthProvider();


    return (<Admin
        defaultTheme='dark'
        basename="/admin"
        dataProvider={dataProvider}
        queryClient={queryClient}
        authProvider={authProvider}
        title="Portmafia"
        disableTelemetry
    >
        <Resource
            name='repo-contribution'
            options={{ label: 'Repo Contributions' }}
            list={RepoContributionList}
            edit={RepoContributionsEdit}
            create={RepoContributionsCreate}
        />
        <Resource
            name='repo'
            options={{ label: 'Repositories' }}
            list={RepoList}
            edit={RepoEdit}
            create={RepoCreate}
            recordRepresentation={(record) => record.name}
        />
    </Admin>
    )
}

export default AdminIndex
