import { Grid } from '@mui/material'
import React from 'react'
import { BooleanInput, Edit, NumberInput, SelectInput, SimpleForm, TextInput } from 'react-admin'

export default function RepoEdit() {
    return (<Edit>
        <SimpleForm>
            <Grid container spacing={2}>
                <Grid item xs>
                    <TextInput source="id" label="Repo ID" readOnly fullWidth />
                </Grid>
                <Grid item xs="auto" style={{ alignItems: "center", display: "flex" }}>
                    <BooleanInput label="Hide" source="is_hidden" />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={4} lg={2}>
                    <SelectInput source="type" choices={[
                        { id: 'github', name: "GitHub" },
                        { id: 'gitlab', name: "GitLab" }
                    ]} isRequired fullWidth />
                </Grid>
                <Grid item xs>
                    <TextInput source="url" isRequired fullWidth />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <TextInput source="host" fullWidth helperText="Can be skipped, will automatically resolve from url." />
                </Grid>
                <Grid item xs>
                    <TextInput source="name" fullWidth helperText="Can be skipped, will automatically resolve from url." />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <NumberInput source="star" fullWidth readOnly />
                </Grid>
            </Grid>
        </SimpleForm>
    </Edit>
    )
}
