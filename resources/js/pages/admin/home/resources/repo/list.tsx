import HideUnhideButtons from "@/fragments/admin/hide-unhide-actions";
import React from "react";
import { BooleanField, Datagrid, EditButton, FunctionField, List, TextField, UrlField, WrapperField } from "react-admin";

export default function RepoList() {
    return (<List>
        <Datagrid rowClick='toggleSelection' bulkActionButtons={<HideUnhideButtons />}>
            <FunctionField label='Repo ID' render={(record: any) => <code>{record.id}</code>} />
            <TextField source='type' />
            <TextField source='name' />
            <TextField source='star' />
            <UrlField label='Link' source="url" target="_blank" rel="noopener noreferrer" />
            <BooleanField label="Hidden" source='is_hidden' />
            <WrapperField label="Actions">
                <EditButton />
            </WrapperField>
        </Datagrid>
    </List>
    )
}
