import { pluckIdFromReferenceInput } from '@/util/admin/admin-utils'
import { Grid } from '@mui/material'
import React from 'react'
import { BooleanInput, DateTimeInput, Edit, ReferenceInput, SelectInput, SimpleForm, TextInput } from 'react-admin'

export default function RepoContributionsEdit() {
    return (<Edit transform={({ updated_at, ...val }: any) => pluckIdFromReferenceInput(val, ['repo'])} mutationMode="pessimistic">
        <SimpleForm>
            <Grid container spacing={2}>
                <Grid item xs>
                    <TextInput source="id" readOnly fullWidth />
                </Grid>
                <Grid item xs="auto" style={{ alignItems: "center", display: "flex" }}>
                    <BooleanInput label="Hide" source="is_hidden" />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <ReferenceInput source="repo.id" label='Repository' reference="repo" required fullWidth helperText="Can be skipped, will automatically resolve from url." />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} md={3} lg={2}>
                    <SelectInput source="type" choices={[
                        { id: 'issue', name: "Issue" },
                        { id: 'mr', name: "Merge Request" },
                    ]} isRequired fullWidth />
                </Grid>
                <Grid item xs={12} md={3} lg={2}>
                    <TextInput source="number" isRequired fullWidth />
                </Grid>
                <Grid item xs>
                    <TextInput source="title" isRequired fullWidth />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <TextInput source="url" isRequired fullWidth />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <DateTimeInput source="created_at" isRequired fullWidth />
                </Grid>
                <Grid item xs>
                    <DateTimeInput source="updated_at" readOnly isRequired fullWidth />
                </Grid>
            </Grid>
        </SimpleForm>
    </Edit>
    )
}
