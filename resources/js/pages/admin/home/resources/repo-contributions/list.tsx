import HideUnhideButtons from '@/fragments/admin/hide-unhide-actions';
import React from 'react'
import { BooleanField, Datagrid, EditButton, FunctionField, List, ReferenceField, TextField, WrapperField } from 'react-admin'

export default function RepoContributionList() {
    return (<List>
        <Datagrid rowClick="toggleSelection" bulkActionButtons={<HideUnhideButtons />}>
            <FunctionField label='Contribution ID' render={(record: any) => <code>{record.id}</code>} />
            <ReferenceField source='repo.id' reference='repo' label='Repository' />
            <TextField source='type' />
            <TextField source='title' />
            <FunctionField label='Link' render={(record: any) => {
                let displayLink = `${record.repo.name}#${record.number}`;
                if (record.repo.type === "gitlab") {
                    displayLink = `${record.repo.name}!${record.number}`;
                }
                return <a href={record.url} rel='noopener' target='_blank'>{displayLink}</a>
            }} />
            <BooleanField label="Hidden" source='is_hidden' />
            <WrapperField label="Actions">
                <EditButton />
            </WrapperField>
        </Datagrid>
    </List>
    )
}
