import BaseLayout from '@/layout/base/base-layout';
import React, { useMemo } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import styles from "./index.module.scss";

function HomeIndexPage() {
    const currentYear = useMemo(() => (new Date()).getFullYear(), []);

    return (
        <BaseLayout>
            <div className={styles.content}>
                <Container>
                    <Row>
                        <Col xs={12} md={8}>
                            <h1>Christianto's Portmafia API</h1>
                            <p>
                                Heyy, you're accessing Chris' Portfolio API. This specific project is not meant
                                to be viewed by public, but if you're curious, go to the repository of this project here: <br />
                                <a href="https://gitlab.com/net.christianto/portmafia-api" target='_blank' rel='noopener noreferrer'>net.christianto/portmafia-api</a>.
                            </p>

                            <div className="mt-5">
                                <p>Or maybe, just go to my website:</p>
                                <Button href="https://christianto.net">Go to Christianto.net</Button>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <footer className='mt-5'>
                    <Container>
                        <p className='small text-muted'>Copyright &copy; 2022-{currentYear} chez14.</p>
                    </Container>
                </footer>
            </div>
        </BaseLayout>
    )
}

export default HomeIndexPage
