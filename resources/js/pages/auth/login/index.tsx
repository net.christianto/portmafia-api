import { PageProps } from '@/models/props/system/page-props'
import React, { useMemo } from 'react'
import { Alert, Button } from 'react-bootstrap'
import { When } from 'react-if'
import styles from "./index.module.scss"
import { Head } from '@inertiajs/react'
import AuthLayout from '@/layout/auth/auth-layout'

export interface LoginIndexProps extends PageProps {
    redir?: string,
    actionUrl: string,
    csrfToken: { name: string, value: string }
}

function LoginIndex({ redir, actionUrl, csrfToken, errors }: LoginIndexProps) {

    let errorBag = errors?.['default'] || [];

    return (
        <AuthLayout title="Login">
            <Head>
                <title>Login to Portmafia</title>
            </Head>
            <div className={styles.content}>
                <div>
                    <h2>Login to Portmafia</h2>

                    {errorBag.map((message, idx) => <Alert variant='danger' key={idx}>{message}</Alert>)}

                    <form method='post' action={actionUrl}>
                        <input type="hidden" name={csrfToken.name} value={csrfToken.value} />
                        <When condition={redir}>
                            <input type="hidden" name="redir" value={redir} />
                        </When>
                        <div>
                            <Button type='submit' className='w-100' size="lg">Login</Button>
                        </div>
                    </form>
                </div>
            </div>
        </AuthLayout>
    )
}

export default LoginIndex
