import { describe, expect, it, test } from 'vitest'
import { objectToParam } from './object-to-param'

describe('objectToParam', () => {
    it('should convert an object to a URL query parameter string', () => {
        const result = objectToParam({ a: '1', b: '2' })
        expect(result).toBe('a=1&b=2')
    })

    it('should convert an object with number values to a URL query parameter string', () => {
        const result = objectToParam({ a: 1, b: 2 })
        expect(result).toBe('a=1&b=2')
    })

    it('should convert an object with boolean values to a URL query parameter string', () => {
        const result = objectToParam({ a: true, b: false })
        expect(result).toBe('a=true&b=false')
    })

    it('should convert an object with nested objects and arrays to a URL query parameter string', () => {
        const result = objectToParam({ a: { nested: 'value' }, b: [1, 2, 3] })
        expect(result).toBe('a=%7B%22nested%22%3A%22value%22%7D&b=%5B1%2C2%2C3%5D')
    })
})
