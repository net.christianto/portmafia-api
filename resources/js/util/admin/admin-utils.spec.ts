import { describe, expect, it } from 'vitest';
import { pluckIdFromReferenceInput } from './admin-utils';

describe('pluckIdFromReferenceInput', () => {
    it('should return an empty object when source is empty', () => {
        const result = pluckIdFromReferenceInput({}, ['ref1', 'ref2']);

        expect(result).to.deep.equal({});
    });

    it('should return the same object when no references to pluck', () => {
        const source = { prop1: 'value1', prop2: 'value2' };

        const result = pluckIdFromReferenceInput(source, ['ref1', 'ref2']);

        expect(result).to.deep.equal(source);
    });

    it('should return the same object when references to pluck do not have id', () => {
        const source = { ref1: 'value1', ref2: 'value2' };

        const result = pluckIdFromReferenceInput(source, ['ref1', 'ref2']);

        expect(result).to.deep.equal(source);
    });

    it('should return an object with ids when references to pluck have id', () => {
        const source = { ref1: { id: 'id1' }, ref2: { id: 'id2' } };

        const result = pluckIdFromReferenceInput(source, ['ref1', 'ref2']);

        expect(result).to.deep.equal({ ref1: 'id1', ref2: 'id2' });
    });
});
