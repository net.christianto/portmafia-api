import { PageProps } from '@/models/props/system/page-props';
import { router, usePage } from '@inertiajs/react';
import { AuthProvider, UserIdentity } from 'react-admin';

/**
 * Options for the authentication provider.
 */
interface AuthProviderOptions {
    /**
     * The URL for the login page.
     */
    loginUrl?: string;

    /**
     * The URL for the logout API to ping.
     */
    logoutUrl?: string;
}

/**
 * Creates an authentication provider object.
 *
 * @param options - The options for the authentication provider.
 * @returns The authentication provider object.
 */
export function useAuthProvider(options: AuthProviderOptions = {}): AuthProvider {
    const { loginUrl = '/auth/login', logoutUrl = '/auth/logout' } = options;

    const {
        props: { user },
    } = usePage<PageProps>();

    return {
        /**
         * Logs the user in.
         *
         * @returns A promise that resolves when the user is logged in.
         */
        login(): Promise<any> {
            router.get(loginUrl);
            return Promise.resolve();
        },

        /**
         * Logs the user out.
         *
         * @returns A promise that resolves with the login URL after the user is logged out.
         */
        logout(): Promise<string | false | void> {
            router.post(logoutUrl);
            return Promise.resolve(loginUrl);
        },

        /**
         * Checks if the user is authenticated.
         *
         * @returns A promise that resolves if the user is authenticated, or rejects otherwise.
         */
        checkAuth(): Promise<void> {
            if (!user) {
                return Promise.reject();
            }

            return Promise.resolve();
        },

        /**
         * Checks for authentication errors.
         *
         * @param error - The error object.
         * @returns A promise that resolves if the error is not an authentication error, or rejects otherwise.
         */
        checkError(error: any): Promise<void> {
            const { status } = error;

            if (status === 401) {
                return Promise.reject();
            }

            return Promise.resolve();
        },

        /**
         * Gets the user's permissions.
         *
         * @param params - The parameters for getting permissions.
         * @returns A promise that resolves with the user's permissions.
         */
        getPermissions(params: any): Promise<any> {
            // TODO: implement this.
            return Promise.resolve(true);
        },

        /**
         * Gets the user's identity.
         *
         * @returns A promise that resolves with the user's identity.
         */
        getIdentity(): Promise<UserIdentity> {
            return Promise.resolve({
                id: user.id,
                fullName: user.name,
            });
        },
    };
}
