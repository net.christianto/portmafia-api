export function pluckIdFromReferenceInput(source: any, referencesToPluck: string[]): any {
    let result: any = {};
    Object.keys(source).forEach((key) => {
        if (referencesToPluck.includes(key) && source[key]?.id) {
            result[key] = source[key]?.id;
        } else {
            result[key] = source[key];
        }
    });

    return result;
}
