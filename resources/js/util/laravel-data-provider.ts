import { GenericApiResponse, GenericPaginatedResponse, PaginatedApiResponse } from '@/models/api/generic-response';
import Cookies from 'js-cookie';
import {
    CreateParams,
    CreateResult,
    DataProvider,
    DeleteManyParams,
    DeleteManyResult,
    DeleteParams,
    DeleteResult,
    GetListParams,
    GetListResult,
    GetManyParams,
    GetManyReferenceParams,
    GetManyReferenceResult,
    GetManyResult,
    GetOneParams,
    GetOneResult,
    HttpError,
    UpdateManyParams,
    UpdateManyResult,
    UpdateParams,
    UpdateResult,
} from 'react-admin';
import { objectToParam } from './object-to-param';

/**
 * Represents the configuration for a data provider.
 */
export interface DataProviderConfig {
    /**
     * The URL to fetch data from.
     */
    url?: string;

    /**
     * The name of the CSRF token cookie.
     */
    csrfTokenCookieName?: string;
}

const defaultDataProviderConfig: DataProviderConfig = {
    url: '/api/rest',
    csrfTokenCookieName: 'XSRF-TOKEN',
};

export function createDataProvider(config: DataProviderConfig = {}): DataProvider {
    const { url, csrfTokenCookieName } = { ...defaultDataProviderConfig, ...config } as Required<DataProviderConfig>;

    const defaultFetchOpt: RequestInit = {
        credentials: 'include',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'X-XSRF-TOKEN': '',
        },
        redirect: 'manual',
        referrerPolicy: 'no-referrer',
    };

    return {
        /**
         * Retrieves a list of resources.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the list of resources.
         */
        async getList(resource: string, params: GetListParams): Promise<GetListResult> {
            let requestUrl = [url, resource].join('/');

            let query: any = {
                page: params.pagination.page || undefined,
                per_page: params.pagination.perPage || undefined,
                sort: params.sort || undefined,
                filter: params.filter || undefined,
            };
            requestUrl += '?' + objectToParam(query);

            let resp = await fetch(requestUrl, {
                ...defaultFetchOpt,
                headers: {
                    ...defaultFetchOpt.headers,
                    'X-XSRF-TOKEN': Cookies.get(csrfTokenCookieName) || '',
                },
                method: 'GET',
            });
            let response: GenericPaginatedResponse = await resp.json();
            if (!resp.ok) {
                throw new HttpError(`Unable to fetch ${resource}: ${response.message}.`, `HTTP${resp.status}`);
            }
            return {
                data: response.data,
                total: (response as PaginatedApiResponse).meta.total,
                pageInfo: {
                    hasNextPage: !!response.links.next,
                    hasPreviousPage: !!response.links.prev,
                },
            };
        },

        /**
         * Retrieves a single resource.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the single resource.
         */
        async getOne(resource: string, params: GetOneParams): Promise<GetOneResult> {
            let resp = await fetch([url, resource, encodeURIComponent(params.id)].join('/'), {
                ...defaultFetchOpt,
                headers: {
                    ...defaultFetchOpt.headers,
                    'X-XSRF-TOKEN': Cookies.get(csrfTokenCookieName) || '',
                },
                method: 'GET',
            });

            let body: GenericApiResponse = await resp.json();
            if (!resp.ok) {
                throw new HttpError(
                    `Unable to fetch ${resource} with id ${params.id}: ${body.message}.`,
                    `HTTP${resp.status}`
                );
            }
            return { data: body.data };
        },

        /**
         * Retrieves multiple resources.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the multiple resources.
         */
        async getMany(resource: string, params: GetManyParams): Promise<GetManyResult> {
            let data = await Promise.all(params.ids.map(async (id) => (await this.getOne(resource, { id })).data));
            return { data };
        },

        /**
         * Retrieves multiple resources based on a reference.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the multiple resources based on the reference.
         * @throws An error indicating that this method is not yet supported.
         */
        getManyReference(resource: string, params: GetManyReferenceParams): Promise<GetManyReferenceResult> {
            // TODO: Support this!
            throw new HttpError('Unsupported method', 'TSCC000', 'getManyReference is not yet supported');
        },

        /**
         * Creates a new resource.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the created resource.
         */
        async create(resource: string, params: CreateParams): Promise<CreateResult> {
            let resp = await fetch([url, resource].join('/'), {
                ...defaultFetchOpt,
                headers: {
                    ...defaultFetchOpt.headers,
                    Accept: 'application/json',
                    'X-XSRF-TOKEN': Cookies.get(csrfTokenCookieName) || '',
                },
                method: 'POST',
                body: JSON.stringify(params.data),
            });
            let data: GenericApiResponse = await resp.json();
            if (!resp.ok) {
                throw new HttpError(`Unable to create new ${resource}: ${data.message}`, `HTTP${resp.status}`);
            }
            return data;
        },

        /**
         * Updates an existing resource.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the updated resource.
         */
        async update(resource: string, params: UpdateParams): Promise<UpdateResult> {
            let resp = await fetch([url, resource, encodeURIComponent(params.id)].join('/'), {
                ...defaultFetchOpt,
                headers: {
                    ...defaultFetchOpt.headers,
                    Accept: 'application/json',
                    'X-XSRF-TOKEN': Cookies.get(csrfTokenCookieName) || '',
                },
                method: 'PUT',
                body: JSON.stringify(params.data),
            });
            let data: GenericApiResponse = await resp.json();
            if (!resp.ok) {
                throw new HttpError(
                    `Unable to update ${resource} with id ${params.id}: ${data.message}.`,
                    `HTTP${resp.status}`
                );
            }
            return data;
        },
        /**
         * Updates multiple resources.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the updated resources.
         */
        async updateMany(resource: string, { ids, data, meta }: UpdateManyParams): Promise<UpdateManyResult> {
            return {
                data: await Promise.all(
                    ids.map(async (id) => (await this.update(resource, { id, data, previousData: null, meta })).data)
                ),
            };
        },

        /**
         * Deletes a resource.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the deleted resource.
         */
        async delete(resource: string, params: DeleteParams): Promise<DeleteResult> {
            let resp = await fetch([url, resource, encodeURIComponent(params.id)].join('/'), {
                ...defaultFetchOpt,
                headers: {
                    ...defaultFetchOpt.headers,
                    Accept: 'application/json',
                    'X-XSRF-TOKEN': Cookies.get(csrfTokenCookieName) || '',
                },
                method: 'DELETE',
            });
            let body: GenericApiResponse = await resp.json();
            if (!resp.ok) {
                throw new HttpError(
                    `Unable to delete ${resource} with id ${params.id}: ${body.message}.`,
                    `HTTP${resp.status}`
                );
            }
            return body;
        },

        /**
         * Deletes multiple resources.
         * @param resource - The resource name.
         * @param params - The parameters for the request.
         * @returns A promise that resolves to the deleted resources.
         */
        async deleteMany(resource: string, params: DeleteManyParams): Promise<DeleteManyResult> {
            return {
                data: await Promise.all(params.ids.map(async (id) => (await this.delete(resource, { id }))?.data)),
            };
        },
    };
}
