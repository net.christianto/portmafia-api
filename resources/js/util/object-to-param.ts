/**
 * Converts an object to a URL query parameter string.
 * @param params - The object containing the parameters.
 * @returns The URL query parameter string.
 */
export function objectToParam(params: Record<string, any>): string {
    return Object.keys(params)
        .map((key) => {
            let val = params[key];
            if (!(typeof val === 'string' || val instanceof String)) {
                val = JSON.stringify(val);
            }

            return [key, val].map(encodeURIComponent).join('=');
        })
        .join('&');
}
