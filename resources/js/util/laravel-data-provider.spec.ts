import { createDataProvider } from '@/util/laravel-data-provider';
import nock from 'nock';
import { GetListParams } from 'react-admin';
import { expect, suite, test } from 'vitest';
import { objectToParam } from './object-to-param';

suite('dataProvider', () => {
    const apiHost = 'http://localhost';
    const apiPath = '/api/rest';
    const resource = 'testResource';

    const dataProvider = createDataProvider({ url: `${apiHost}${apiPath}` });

    test('getList', async () => {
        const options: GetListParams = {
            pagination: { page: 1, perPage: 10 },
            sort: { field: 'id', order: 'ASC' },
            filter: {},
        };
        const expectedQuery = {
            page: options.pagination.page,
            per_page: options.pagination.perPage,
            sort: options.sort,
            filter: options.filter,
        };
        nock(apiHost)
            .get(`${apiPath}/${resource}?${objectToParam(expectedQuery)}`)
            .reply(200, { data: [{ id: 1, name: 'Test' }], meta: { total: 1 }, links: { next: null, prev: null } });

        const result = await dataProvider.getList(resource, options);
        expect(result.data).toHaveLength(1);
        expect(result.total).toBe(1);
    });

    test('getOne', async () => {
        nock(apiHost)
            .get(`${apiPath}/${resource}/1`)
            .reply(200, { data: { id: 1, name: 'Test' } });

        const result = await dataProvider.getOne(resource, { id: '1' });
        expect(result.data).toEqual({ id: 1, name: 'Test' });
    });

    test('getMany', async () => {
        nock(apiHost)
            .get(`${apiPath}/${resource}/1`)
            .reply(200, { data: { id: 1, name: 'Test' } });

        const result = await dataProvider.getMany(resource, { ids: ['1'] });
        expect(result.data).toHaveLength(1);
        expect(result.data[0]).toEqual({ id: 1, name: 'Test' });
    });

    test('create', async () => {
        nock(apiHost)
            .post(`${apiPath}/${resource}`)
            .reply(201, { data: { id: 2, name: 'Test 2' } });

        const result = await dataProvider.create(resource, { data: { name: 'Test 2' } });
        expect(result.data).toEqual({ id: 2, name: 'Test 2' });
    });

    test('update', async () => {
        nock(apiHost)
            .put(`${apiPath}/${resource}/1`)
            .reply(200, { data: { id: 1, name: 'Test Updated' } });

        const result = await dataProvider.update(resource, {
            id: '1',
            data: { name: 'Test Updated' },
            previousData: {},
        });
        expect(result.data).toEqual({ id: 1, name: 'Test Updated' });
    });

    test('delete', async () => {
        nock(apiHost)
            .delete(`${apiPath}/${resource}/1`)
            .reply(200, { data: { id: 1, name: 'Test Deleted' } });

        const result = await dataProvider.delete(resource, { id: '1' });
        expect(result.data).toEqual({ id: 1, name: 'Test Deleted' });
    });
});
