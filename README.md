# Portmafia API

Welcome to Portmafia API, a Laravel-based project that hosts APIs for managing
your personal website including portfolio, contacts, and more. The name
"Portmafia" is inspired by the anime series Bungou Stray Dogs.

## Features

- Provides APIs for managing portfolio, contacts, and other website-related
  functionalities.
- Utilizes Laravel framework for backend development.
- Deployed with Docker for easy scalability and portability.
- Uses MariaDB as the database management system.
- Admin panel frontend is built with React.js and TypeScript.
- Utilizes a themed Bootstrap called Plong for styling.
- Implements Inertia.js for seamless binding between PHP and JavaScript.
- Bundled using Vite.js for efficient development workflow.
- Laravel Sail is provided for development Docker environment.
- Implements Zitadel for authentication.
- Utilizes Laravel Passport for server-to-server authentication.

## Installation

1. Clone the repository:

    ```bash
    git clone <repository_url>
    ```

2. Install dependencies:

    ```bash
    cd portmafia-api
    composer install
    yarn install
    ```

3. Set up environment variables:

    ```bash
    cp .env.example .env
    ```

    Edit `.env` file and set up necessary environment variables such as database
    credentials, Zitadel configuration, etc.

4. Generate Laravel application key:
    ```bash
    php artisan key:generate
    ```
    This will generate a unique application key which is used for encryption, hashing, and other security-related features.



1. Run migrations and seeders:

    ```bash
    php artisan migrate --seed
    ```

5. Run the development server:

    ```bash
    ./vendor/bin/sail up
    ```

## Usage

Once the development server is running, you can access the API endpoints and
admin panel on http://localhost:8000/api and http://localhost:8000/admoon.


## License

This project is licensed under the [AGPLv3 License](LICENSE).

