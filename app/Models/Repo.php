<?php

namespace App\Models;

use App\Enums\RepoType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Malico\LaravelNanoid\HasNanoids;

/**
 * The repo model
 *
 * @property string $id
 * @property string $name
 * @property string $link
 * @property int $star
 */
class Repo extends Model
{
    use HasFactory, HasNanoids;

    protected $nanoidPrefix = 'repo::';

    protected $fillable = [
        'link',
        'star',
        'is_hidden',
        'type'
    ];

    protected $casts = [
        'is_hidden' => 'boolean',
        'type' => RepoType::class
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($repo) {
            $repo->populateHostAndName();
        });

        static::updating(function ($repo) {
            $repo->populateHostAndName();
        });
    }

    /**
     * Populate the host and name attributes from the link
     *
     * @return void
     */
    public function populateHostAndName()
    {
        if (!empty($this->link)) {
            $urlParts = parse_url($this->link);
            $this->host = $urlParts['host'];
            $this->name = trim($urlParts['path'], "/");
        }
    }

    public function contributions(): HasMany
    {
        return $this->hasMany(RepoContribution::class);
    }

    public function scopeShown(Builder $query)
    {
        $query->where('is_hidden', false);
    }
}
