<?php

namespace App\Models;

use App\Enums\RepoContributionType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Malico\LaravelNanoid\HasNanoids;

class RepoContribution extends Model
{
    use HasFactory, HasNanoids;

    protected $nanoidPrefix = 'ctrb::';

    protected $fillable = [
        'repo_id',
        'title',
        'url',
        'type',
        'number',
        'is_hidden',
        'created_at'
    ];

    protected $casts = [
        'type' => RepoContributionType::class,
        'is_hidden' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->autoPopulateColumns();
        });
    }

    public function autoPopulateColumns()
    {
        if ($this->url) {
            $repoInfo = $this->getRepoInfo($this->url);
            $repo = Repo::firstOrCreate(['link' => $repoInfo['repo']]);
            $this->repo_id = $repo->id;
            $this->type = $repoInfo['type'];
            if (!$this->number) {
                $this->number = $repoInfo['number'];
            }
        }
    }

    function getRepoInfo($url)
    {
        preg_match('/(?<repo>.*)\/(-\/)?(?<type>issues|pull|merge_requests)\/?(?<number>[\d]*)\/?$/U', $url, $matches);

        $type = RepoContributionType::MergeRequest;
        if ($matches['type'] == 'issues') {
            $type = RepoContributionType::Issue;
        }
        return [
            "repo" => $matches['repo'],
            "type" => $type,
            "number" => $matches['number'],
        ];
    }

    public function repo()
    {
        return $this->belongsTo(Repo::class);
    }

    public function scopeShown(Builder $query)
    {
        $query->whereHas('repo', function (Builder $query) {
            $query->shown();
        });
        $query->where('is_hidden', false);
    }
}
