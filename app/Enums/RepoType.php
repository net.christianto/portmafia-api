<?php

namespace App\Enums;

enum RepoType: string
{
    case GitLab = "gitlab";
    case GitHub = "github";
}
