<?php

namespace App\Enums;

enum RepoContributionType: string
{
    case MergeRequest = "mr";
    case Issue = "issue";
}
