<?php

namespace App\Enums;

enum AuthScope: string
{
    case Repo_Show = 'repo:show';
    case Repo_Create = 'repo:create';
    case Repo_Delete = 'repo:delete';
    case Repo_Update = 'repo:update';

    case Contrib_Show = 'contrib:show';
    case Contrib_Create = 'contrib:create';
    case Contrib_Delete = 'contrib:delete';
    case Contrib_Update = 'contrib:update';

    case User_Read = 'user:show';
    case User_Write = 'user:write';
}
