<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GitlabApi
{
    public static function instance($token = null, $domain = "https://gitlab.com")
    {
        $token = $token ?? env('GL_PERSONAL_TOKEN', "");

        $client = new Client([
            "base_uri" => "{$domain}/api/v4/",
            "headers" => [
                'Authorization' => "Bearer {$token}",
                'Accept' => 'application/vnd.github+json'
            ]
        ]);

        return $client;
    }
}
