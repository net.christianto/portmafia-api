<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GithubApi
{
    public static function instance()
    {
        $token = env('GH_PERSONAL_TOKEN', "");

        $client = new Client([
            "base_uri" => "https://api.github.com/",
            "headers" => [
                'Authorization' => "Bearer {$token}",
                'Accept' => 'application/vnd.github+json'
            ]
        ]);

        return $client;
    }
}
