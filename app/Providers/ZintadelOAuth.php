<?php

namespace App\Providers;

use CHEZ14\OAuth2\Client\Provider\ZitadelProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ZintadelOAuth extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(ZitadelProvider::class, function (Application $app) {
            return new ZitadelProvider([
                'clientId' => env('AUTH_CLIENT_ID'),
                'clientSecret' => env('AUTH_CLIENT_SECRET'),
                'pkceMethod' => \League\OAuth2\Client\Provider\GenericProvider::PKCE_METHOD_S256,
                'domain' => env('AUTH_DOMAIN', 'https://digivert-vhs6jl.zitadel.cloud'),
                'redirectUri' => route('auth:callback'),
            ]);
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
