<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Response::macro('restFormat', function ($value, $status = 200, array $headers = [], string $message = null) {
            $base = [
                'status' => floor($status / 100) == 2,
                'message' => $message
            ];

            if ($value instanceof JsonResource) {
                return $value->additional($base);
            }

            return response()->json([
                ...$base,
                'data' => $value,
            ], $status, $headers);
        });
    }
}
