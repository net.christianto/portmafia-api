<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Enums\AuthScope;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $tokens = [
            AuthScope::Repo_Create->value => "Create Repositories",
            AuthScope::Repo_Delete->value => "Delete Repositories",
            AuthScope::Repo_Show->value => "Show Repositories",
            AuthScope::Repo_Update->value => "Update Repositories",
            AuthScope::Contrib_Create->value => "Create Contributions",
            AuthScope::Contrib_Delete->value => "Delete Contributions",
            AuthScope::Contrib_Show->value => "Show Contributions",
            AuthScope::Contrib_Update->value => "Update Contributions",
        ];
        Passport::tokensCan($tokens);
    }
}
