<?php

namespace App\Http\Controllers\Api\Management;

use App\Http\Requests\Management\StoreRepoContributionRequest;
use App\Http\Requests\Management\UpdateRepoContributionRequest;
use App\Http\Resources\RepoContributionResource;
use App\Models\RepoContribution as ModelRepoContribution;
use Illuminate\Http\Request;

class RepoContribution extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $repos = ModelRepoContribution::orderBy('created_at', 'desc');

        return response()->restFormat(
            RepoContributionResource::collection(
                $repos->paginate()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRepoContributionRequest $request)
    {

        $repoContribution = ModelRepoContribution::create($request->validated());

        return response()->restFormat(RepoContributionResource::make($repoContribution), 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(ModelRepoContribution $repoContribution)
    {
        return response()->restFormat(RepoContributionResource::make($repoContribution));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRepoContributionRequest $request, ModelRepoContribution $repoContribution)
    {

        $repoContribution->update($request->validated());

        return response()->restFormat(RepoContributionResource::make($repoContribution), 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ModelRepoContribution $repoContribution)
    {
        $repoContribution->delete();

        return response()->restFormat(RepoContributionResource::make($repoContribution), 204);
    }
}
