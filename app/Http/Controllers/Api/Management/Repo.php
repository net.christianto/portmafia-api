<?php

namespace App\Http\Controllers\Api\Management;

use App\Http\Requests\Management\StoreRepoRequest;
use App\Http\Requests\Management\UpdateRepoRequest;
use App\Http\Resources\RepoResource;
use App\Models\Repo as ModelRepo;

class Repo extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $repos = ModelRepo::orderBy('created_at', 'desc');

        return response()->restFormat(RepoResource::collection($repos->paginate()));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRepoRequest $request)
    {
        $validated = $request->validated();
        if (isset($validated['url'])) {
            $validated['link'] = $validated['url'];
            unset($validated['url']);
        }
        $repo = ModelRepo::create($validated);

        return response()->restFormat(RepoResource::make($repo), 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(ModelRepo $repo)
    {
        return response()->restFormat(RepoResource::make($repo));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRepoRequest $request, ModelRepo $repo)
    {
        $validated = $request->validated();
        if (isset($validated['url'])) {
            $validated['link'] = $validated['url'];
            unset($validated['url']);
        }
        $repo->fill($validated);
        $repo->save();

        return response()->restFormat(RepoResource::make($repo), 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ModelRepo $repo)
    {
        $repo->delete();

        return response()->restFormat(RepoResource::make($repo), 204);
    }
}
