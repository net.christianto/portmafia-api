<?php

namespace App\Http\Controllers;

use App\Enums\RepoType;
use App\Helpers\GithubApi;
use App\Helpers\GitlabApi;
use App\Http\Requests\RepoContributionStoreRequest;
use App\Http\Requests\RepoContributionUpdateRequest;
use App\Http\Requests\StoreRepoContributionRequest;
use App\Http\Requests\StoreRepoContributionWithResolverRequest;
use App\Http\Requests\UpdateRepoContributionRequest;
use App\Http\Resources\RepoContributionResource;
use App\Http\Resources\RepoResource;
use App\Models\RepoContribution;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;

class RepoContributions extends Controller
{
    public function serve()
    {
        $paginated =  RepoContributionResource::collection(
            RepoContribution::shown()
                ->orderBy('created_at', 'desc')
                ->cursorPaginate()
        );

        return response()->restFormat($paginated);
    }
}
