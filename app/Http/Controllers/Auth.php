<?php

namespace App\Http\Controllers;

use App\Models\User;
use CHEZ14\OAuth2\Client\Provider\ZitadelProvider;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;

class Auth extends Controller
{
    const SESSION_PKCE = "auth:pkce";
    const SESSION_STATE = "auth:state";

    public function login()
    {
        if (auth()->hasUser()) {
            return redirect()->intended(route('admin:home'));
        }

        return Inertia::render('auth/login/index', [
            'csrfToken' => ['name' => '_token', 'value' => csrf_token()]
        ]);
    }

    public function loginDo(ZitadelProvider $provider)
    {
        $authUrl = $provider->getAuthorizationUrl([
            'scope' => join(' ', [
                'openid',
                'profile',
                'email',
                'offline_access',
                ...explode(' ', env('AUTH_EXTRA_SCOPE', ''))
            ])
        ]);

        $pkce = $provider->getPkceCode();
        $state = $provider->getState();

        session([
            self::SESSION_PKCE => $pkce,
            self::SESSION_STATE => $state
        ]);

        return redirect($authUrl);
    }

    function logout(): RedirectResponse
    {
        auth()->logout();

        session()->invalidate();

        session()->regenerateToken();

        return redirect()->intended(route('auth:login'));
    }

    function callback(ZitadelProvider $provider)
    {
        if (request('state') != session(self::SESSION_STATE)) {
            return to_route('auth:login')->withErrors('Invalid state id, please login again.');
        }

        $pkce = session(self::SESSION_PKCE);
        $provider->setPkceCode($pkce);
        $tokenInfo = null;

        try {
            $token = $provider->getAccessToken('authorization_code', [
                'code' => request('code')
            ]);

            $tokenInfo = $provider->getResourceOwner($token)->toArray();
        } catch (IdentityProviderException $e) {
            return to_route('auth:login')->withErrors($e->getMessage());
        }

        try {
            $user = User::where([
                'email' => $tokenInfo['email']
            ])->firstOrFail();

            $user->name = $tokenInfo['name'];
            $user->save();
            auth()->login($user);
        } catch (ModelNotFoundException $e) {
            return to_route('auth:login')->withErrors("Email '{$tokenInfo['email']}' is not registered in the system. System is invite-only.");
        }

        return redirect()->intended(route('admin:home'));
    }
}
