<?php

namespace App\Http\Requests\Management;

use App\Enums\RepoContributionType;
use App\Models\RepoContribution;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRepoContributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'number' => ['nullable', 'string'],
            'repo' => ['nullable', 'exists:repos,id'],
            'title' => ['string'],
            'is_hidden' => ['nullable', 'boolean'],
            'type' => [Rule::enum(RepoContributionType::class)],
            'url' => ['url', Rule::unique(RepoContribution::class)->ignore($this->route('repo_contribution'))],
            'created_at' => ['date'],
            'updated_at' => ['missing']
        ];
    }
}
