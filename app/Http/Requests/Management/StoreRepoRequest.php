<?php

namespace App\Http\Requests\Management;

use App\Enums\RepoType;
use App\Models\Repo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRepoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'url' => ['required', 'url', Rule::unique(Repo::class, 'link')],
            'host' => ['nullable', 'string'],
            'is_hidden' => ['nullable', 'boolean'],
            'name' => ['nullable', 'string'],
            'star' => ['nullable', 'numeric'],
            'type' => ['required', Rule::enum(RepoType::class)],
        ];
    }
}
