<?php

namespace App\Http\Requests\Management;

use App\Enums\RepoContributionType;
use App\Models\RepoContribution;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRepoContributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'number' => ['nullable', 'string'],
            'repo' => ['nullable', 'exists:repos,id'],
            'title' => ['required', 'string'],
            'is_hidden' => ['nullable', 'boolean'],
            'type' => ['required', Rule::enum(RepoContributionType::class)],
            'url' => ['required', 'url', Rule::unique(RepoContribution::class)],
            'created_at' => ['date'],
            'updated_at' => ['missing']
        ];
    }
}
