<?php

namespace App\Http\Requests;

use App\Models\RepoContribution;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRepoContributionWithResolverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "url" => ["required", "array", "min:1", "max:15"],
            "url.*" => ["distinct:ignore_case", "url", Rule::unique(RepoContribution::class, 'url')],
        ];
    }
}
