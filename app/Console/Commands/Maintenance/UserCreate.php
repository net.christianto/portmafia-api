<?php

namespace App\Console\Commands\Maintenance;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {--no-password : Indicates that the user should have an empty password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask('Enter the name of the user');
        $email = $this->ask('Enter the email of the user');
        $password = Str::random(10);

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        if (!$this->option('no-password')) {
            $user->password = $password;
        }
        $user->save();

        $this->info('User created successfully!');

        // Display user information
        $this->line('User ID: ' . $user->id);
        $this->line('Name: ' . $user->name);
        $this->line('Email: ' . $user->email);
        if (!$this->option('no-password')) {
            $this->line('Password: ' . $password);
        }
    }
}
