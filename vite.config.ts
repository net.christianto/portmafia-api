import laravel from 'laravel-vite-plugin';
import { mergeConfig } from 'vite';
import manifestSRI from 'vite-plugin-manifest-sri';
import viteSharedConfig from './vite-shared.config';

export default mergeConfig(viteSharedConfig, {
    plugins: [
        laravel({
            input: ['resources/css/app.scss', 'resources/js/app.tsx'],
            refresh: true,
        }),
        manifestSRI(),
    ],
    esbuild: {
        legalComments: 'none',
    },
    build: {
        terserOptions: {
            format: {
                comments: false,
            },
        },
    },
});
