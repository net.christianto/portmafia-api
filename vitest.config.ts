import { configDefaults, mergeConfig } from 'vitest/config';
import viteConfig from './vite-shared.config';

export default mergeConfig(viteConfig, {
    test: {
        include: ['resources/js/**/*.{spec,test}.ts'],
        exclude: [...configDefaults.exclude, 'resources/js/pages/**'],
    },
});
