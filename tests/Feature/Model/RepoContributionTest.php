<?php

namespace Tests\Feature\Model;

use App\Enums\RepoContributionType;
use App\Models\RepoContribution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RepoContributionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $repoContribution = new RepoContribution();
        $url = 'https://github.com/example/repository/issues/123';
        $repoContribution->url = $url;

        $repoContribution->autoPopulateColumns();
        $this->assertEquals(RepoContributionType::Issue, $repoContribution->type);
        $this->assertEquals(123, $repoContribution->number);

        $this->assertNotNull($repoContribution->repo);
        $this->assertEquals('https://github.com/example/repository', $repoContribution->repo->link);
    }
}
