<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->string('new_user_id')->nullable()->after('id');
        });

        DB::statement('UPDATE oauth_clients SET new_user_id = user_id');

        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->renameColumn('new_user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->bigInteger('new_user_id')->nullable()->after('id');
        });

        DB::statement('UPDATE oauth_clients SET new_user_id = user_id');

        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->renameColumn('new_user_id', 'user_id');
        });
    }
};
