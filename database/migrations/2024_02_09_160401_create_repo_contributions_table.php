<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('repo_contributions', function (Blueprint $table) {
            $table->string('id')->primary();

            $table->string('repo_id');
            $table->foreign('repo_id')
                ->references('id')
                ->on('repos');

            $table->string('title');
            $table->string('url')
                ->unique();
            $table->enum('type', ['mr', 'issue']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('repo_contributions');
    }
};
