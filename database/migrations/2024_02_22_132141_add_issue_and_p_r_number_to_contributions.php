<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('repo_contributions', function (Blueprint $table) {
            $table->string('number')
                ->after('title')
                ->nullable()
                ->comment('The Issue/PR number');
        });
        DB::table('repo_contributions')->update([
            'number' => DB::raw("SUBSTRING_INDEX(url, '/', -1)")
        ]);
        Schema::table('repo_contributions', function (Blueprint $table) {
            $table->string('number')
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('repo_contributions', function (Blueprint $table) {
            $table->dropColumn('number');
        });
    }
};
