<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('repos', function (Blueprint $table) {
            $table->boolean('is_hidden')
                ->default(0)
                ->after('name');

            $table->enum('type', ['gitlab', 'github'])
                ->nullable()
                ->after('is_hidden');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('repos', function (Blueprint $table) {
            $table->dropColumn(['is_hidden', 'type']);
        });
    }
};
