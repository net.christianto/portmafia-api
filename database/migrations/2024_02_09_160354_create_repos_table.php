<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('repos', function (Blueprint $table) {
            $table->string('id')
                ->primary();
            $table->string('host')
                ->nullable()
                ->comment('Repository host');
            $table->string('link')
                ->comment('Repository full link, should be clickable by user, directly link to GH/GL page')
                ->unique();
            $table->string('name')
                ->comment('Repository name');
            $table->integer('star')
                ->nullable()
                ->comment('Repo star count');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('repos');
    }
};
